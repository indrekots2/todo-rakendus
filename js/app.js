(function () {
    document.addEventListener('DOMContentLoaded', event => {
        document.getElementById('todoItemInput').addEventListener('submit', addTodoItem);
        document.getElementById('deleteAllInput').addEventListener('submit', deleteAll);
        fetchTodoItems();
    });

    function fetchTodoItems() {
        fetch('?cmd=view')
            .then(response => response.json())
            .then(populateTodoItemTable);

    }

    function populateTodoItemTable(items) {
        for (let item of items) {
            appendToTodoItemTable(item);
        }

    }

    function appendToTodoItemTable(item) {
        let table = document.getElementById('todoItems').getElementsByTagName('tbody')[0];
        let row = table.insertRow();
        addTextCell(row, item.id);
        addTextCell(row, item.name);
        addTextCell(row, item.dateAdded);
        addCell(row, createDeleteLink(item.id));
    }

    function addTodoItem(event) {
        event.preventDefault();
        let todoItemName = document.getElementById('todoItemInput').todoItem.value;
        let payload = {
            name: todoItemName
        };
        if (todoItemName) {
            post('?cmd=add', payload)
                .then(todoItem => {
                    clearInputForm();
                    appendToTodoItemTable(todoItem);
                })
                .catch(error => displayErrors(error.errors));
        }
    }

    function displayErrors(errors) {
        let errorContainer = document.getElementById('errors');
        let list = document.createElement('ul');
        errorContainer.appendChild(list);
        for (let error of errors) {
            addError(error, list);
        }
    }

    function addError(error, container) {
        let listItem = document.createElement('li');
        listItem.appendChild(document.createTextNode(error));
        container.appendChild(listItem);
    }

    function clearErrors() {
        let errorList = document.getElementById('errors').getElementsByTagName('ul')[0];
        if (errorList) {
            errorList.remove();
        }
    }

    function deleteAll(event) {
        event.preventDefault();
        post('?cmd=delete_all').then(clearTodoItems);
    }

    function clearTodoItems() {
        let tbody = document.getElementById('todoItems').getElementsByTagName('tbody')[0];
        while (tbody.hasChildNodes()) {
            tbody.removeChild(tbody.firstChild);
        }
    }

    function clearInputForm() {
        document.getElementById('todoItemInput').reset();
    }

    function addTextCell(row, value) {
        let cell = row.insertCell();
        cell.appendChild(document.createTextNode(value));
    }

    function addCell(row, node) {
        let cell = row.insertCell();
        cell.appendChild(node);
    }

    function createDeleteLink(todoItemid) {
        let link = document.createElement('a');
        link.appendChild(document.createTextNode('Valmis'));
        link.setAttribute('href', '#');
        link.addEventListener('click', event => {
            event.preventDefault();
            post(`?cmd=delete&id=${todoItemid}`)
                .then(() => link.closest('tr').remove());
        });

        return link;
    }

    function post(url, payload) {
        clearErrors();

        let options = {
            method: 'POST'
        };

        if (payload) {
            options.headers = {'Content-Type': 'application/json'};
            options.body = JSON.stringify(payload);
        }

        return new Promise((resolve, reject) => {
            fetch(url, options)
                .then(parseJson)
                .then(response => {
                    if (response.ok) {
                        resolve(response.data);
                    } else {
                        reject(response.data);
                    }
                })
        });

        function parseJson(response) {
            return new Promise((resolve, reject) => {
                response.json()
                    .then(data => {
                        console.log('json', data);
                        resolve({ok: response.ok, data})
                    })
                    .catch(() => {
                        resolve({ok: response.ok})
                    })
            });
        }

    }
})();



