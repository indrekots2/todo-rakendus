<?php
require_once("todoItemDao.php");
require_once("todoItem.php");
require_once("sqliteTodoItemDao.php");

$cmd = isset($_GET["cmd"]) ? $_GET["cmd"] : "";
$todoItemDao = new SqliteTodoItemDao();

function print_json($data) {
    header("Content-Type: application/json");
    print json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
}

function print_client_error($data) {
    header("Content-Type: application/json");
    http_response_code(400);
    print json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
}
if ($cmd == "view") {

    $todoItems = $todoItemDao->findAll();
    print_json($todoItems);

 } else if ($cmd == "add" && $_SERVER['REQUEST_METHOD'] === "POST") {
    $input = json_decode(file_get_contents("php://input"), true);
    $todoItem = TodoItem::fromAssocArray($input);
    $errors = $todoItem->validate();

    if (count($errors) == 0) {
        $todoItem = $todoItemDao->save($todoItem);
        print_json($todoItem);
    } else {
        $data['errors'] = $errors;
        http_response_code(400);
        print_json($data);
    }
} else if ($cmd == "delete" && $_SERVER['REQUEST_METHOD'] === "POST") {

    if (isset($_GET["id"])) {
        $todoItemDao->deleteById($_GET["id"]);
    }
    http_response_code(204);

} else if ($cmd == "delete_all" && $_SERVER['REQUEST_METHOD'] === "POST") {
    $todoItemDao->deleteAll();
    http_response_code(204);
} else if ($cmd == "edit") {
    $input = json_decode(file_get_contents("php://input"), true);
    $todoItem = TodoItem::fromAssocArray($input);

    $errors = $todoItem->validate();

    if (count($errors) == 0) {

        try {
            $todoItem = $todoItemDao->update($todoItem);
            print_json($todoItem);
        } catch (InvalidArgumentException $e) {
            $data["errors"] = [$e->getMessage()];
            print_client_error($data);
        }

    } else {
        $data['errors'] = $errors;
        http_response_code(400);
        print_json($data);
    }
} else {
    readfile("html/main.html");
}





