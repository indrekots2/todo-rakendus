<?php

function add_todo_item($name) {
    if (isset($_SESSION["todoItems"])) {
        $todoItems = $_SESSION["todoItems"];
        if (!in_array($name, $todoItems)) {
            $_SESSION["todoItems"][] = $name;
        }
    } else {
        $_SESSION["todoItems"] = [$name];
    }
}

function get_todo_items() {
    return isset($_SESSION["todoItems"]) ? $_SESSION["todoItems"] : [];
}

function delete_all_todo_items() {
    unset($_SESSION["todoItems"]);
}

function delete_todo_item($name) {
    $todoItems = get_todo_items();
    $itemIndex = array_search($name, $todoItems);
    if ($itemIndex >= 0) {
        unset($todoItems[$itemIndex]);
        $_SESSION["todoItems"] = $todoItems;
    }
}