<?php

class SqliteTodoItemDao implements TodoItemDao {

    const URL = "sqlite:todoApp.sqlite";

    private $connection;

    function __construct() {
        $this->connection = new PDO(self::URL);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    }

    function save($todoItem) {
        $statement = $this->connection->prepare('insert into todo_item (name, date_added) values (:name, :date)');
        $statement->bindValue(':name', $todoItem->name);
        $statement->bindValue(':date', $todoItem->dateAdded);
        $statement->execute();

        $id = $this->connection->lastInsertId();

        foreach ($todoItem->comments as $comment) {
            $this->addComment($comment, $id);
        }

        return $this->findById($id);
    }

    private function addComment($comment, $todoItemId) {
        $statement = $this->connection->prepare('insert into comment (todo_item_id, content) values (:id, :content)');
        $statement->bindValue(':id', $todoItemId);
        $statement->bindValue(':content', $comment);
        $statement->execute();
    }

    function findById($id) {
        $statement = $this->connection->prepare('
          select t.id, t.name, t.date_added, c.content from todo_item t
          left join comment c on c.todo_item_id = t.id 
          where t.id = :id;');
        $statement->bindValue(":id", $id);
        $statement->execute();

        $todoItem = null;
        foreach ($statement as $row) {
            if ($todoItem != null) {
                $todoItem->add_comment($row['content']);
            } else {
                $todoItem = new TodoItem($row['name'], $row['date_added'], $row['id']);
                $todoItem->add_comment($row['content']);
            }
        }

        return $todoItem;
    }

    function findAll() {
        $statement = $this->connection->prepare('
          select * from todo_item 
          left join comment on comment.todo_item_id = todo_item.id;');
        $statement->execute();

        $todoItems = [];
        foreach ($statement as $row) {
            $id = $row['id'];
            if (isset($todoItems[$id])) {
               // todoitem on juba olemas, lisame sellele kommentaari
                $item = $todoItems[$id];
                $item->add_comment($row['content']);
            } else {
               // uus todoitem
                $item = new TodoItem($row['name'], $row['date_added'], $row['id']);
                $item->add_comment($row['content']);
                $todoItems[$item->id] = $item;
            }
        }

        return array_values($todoItems);
    }

    function deleteAll() {
        $statement = $this->connection->prepare('delete from todo_item');
        $statement->execute();
    }

    function deleteById($id) {
        $statement = $this->connection->prepare('delete from todo_item where id = :id');
        $statement->bindValue(':id', $id);
        $statement->execute();
    }

    function update($todoItem) {
        if (!isset($todoItem->id)) {
            throw new InvalidArgumentException("ID väli puudub");
        }

        if ($this->findById($todoItem->id) == null) {
            throw new InvalidArgumentException("TodoItem'it ei ole olemas (id: $todoItem->id)");
        }

        $this->deleteTodoItemComments($todoItem);

        $statement = $this->connection->prepare('
            update todo_item 
            set name = :name, date_added = :dateAdded 
            where id = :id');
        $statement->bindValue(":name", $todoItem->name);
        $statement->bindValue(":dateAdded", $todoItem->dateAdded);
        $statement->bindValue(":id", $todoItem->id);
        $statement->execute();

        foreach ($todoItem->comments as $comment) {
            $this->addComment($comment, $todoItem->id);
        }

        return $this->findById($todoItem->id);
    }

    private function deleteTodoItemComments($todoItem) {
        $statement = $this->connection->prepare('delete from comment where todo_item_id = :id');
        $statement->bindValue(':id', $todoItem->id);
        $statement->execute();
    }

}
