create table todo_item (
  id integer PRIMARY KEY AUTOINCREMENT,
  name varchar(255),
  date_added text
);

create table comment (
  todo_item_id integer,
  content text
);