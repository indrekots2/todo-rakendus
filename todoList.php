<?php

const DATA_FILE = "data.txt";

function add_todo_item($name) {
    if (isset($name) && $name !== "") {
        $todoItems = get_todo_items();
        if (!in_array($name, $todoItems)) {
            file_put_contents(DATA_FILE, $name . PHP_EOL, FILE_APPEND);
        }
    }
}

function get_todo_items() {
    return file(DATA_FILE, FILE_IGNORE_NEW_LINES);
}

function delete_all_todo_items() {
    file_put_contents(DATA_FILE, "");
}

function delete_todo_item($name) {
    $todoItems = get_todo_items();
    delete_all_todo_items();
    foreach ($todoItems as $item) {
        if ($item != $name) {
           add_todo_item($item);
        }
    }
}
